package in.xebia.ems.testData;

import in.xebia.ems.domain.Employee;
import in.xebia.ems.domain.Project;
import in.xebia.ems.domain.Role;
import in.xebia.ems.utils.DateUtils;

import java.util.Date;
import java.util.List;

public class DataGenerator {

	public static Employee createEmployee(String username, String password,
			String firstName, String lastName, String email, Boolean staff,
			Boolean active, Date lastLogin, Date dateJoined, List<Role> roles) {
		Employee employee = new Employee();
		employee.setUsername(username);
		employee.setPassword(password);
		employee.setFirstName(firstName);
		employee.setLastName(lastName);
		employee.setEmail(email);
		employee.setStaff(staff);
		employee.setActive(active);
		employee.setLastLogin(lastLogin);
		employee.setDateJoined(dateJoined);
		employee.setRoles(roles);
		return employee;
	}

	public static Role createRole(String rolename) {
		Role role = new Role();
		role.setRoleName(rolename);
		return role;
	}
	
	public static Employee createEmployeeWithDefaultData(){
		Employee employee = new Employee();
		employee.setUsername("defaultUserName");
		employee.setPassword("defaultPassword");
		employee.setFirstName("defaultFName");
		employee.setLastName("defaultLname");
		employee.setEmail("defaultEmail");
		employee.setStaff(true);
		employee.setActive(true);
		employee.setLastLogin(DateUtils.toTimeStamp("01-01-2010 00:00:00"));
		employee.setDateJoined(DateUtils.toDate("01-01-2010"));
		return employee;		
	}
	
	public static Role createRoleWithDefaultData(){
		Role role = new Role();
		role.setRoleName("ROLE_DEFAULT");
		return role;
	}
	
	public static Project createProject(String projectName) {
		Project project = new Project();
		project.setProjectName(projectName);
		return project;
	}

}
