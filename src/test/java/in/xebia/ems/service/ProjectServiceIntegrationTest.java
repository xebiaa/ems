package in.xebia.ems.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import in.xebia.ems.domain.Project;
import in.xebia.ems.testData.DataGenerator;

import javax.annotation.Resource;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

@ContextConfiguration("/applicationContext-test.xml")
public class ProjectServiceIntegrationTest extends AbstractTransactionalJUnit4SpringContextTests{
	
	@Resource(name="projectService")
	ProjectService projectService;
	
	
	@Test
	public void shouldSaveRole(){
		projectService.saveProject(DataGenerator.createProject("ICC"));
		Project savedProject = projectService.findProjectByName("ICC");
		assertNotNull(savedProject);
	}
	
	@Test
	public void shouldDeleteProject(){
		Project project1 = DataGenerator.createProject("NEW");
		projectService.saveProject(project1);
		Project project2 = projectService.findProjectByName("NEW");
		projectService.removeProject(project2);
		assertNull(projectService.findProjectByName("NEW"));
	}
	
	@Test
	public void shouldListAllEmployees(){
		Project project1 = DataGenerator.createProject("ABC");
		Project project2 = DataGenerator.createProject("DEF");
		Project project3 = DataGenerator.createProject("XYZ");
		
		projectService.saveProject(project1);
		projectService.saveProject(project2);
		projectService.saveProject(project3);
		
		assertEquals(3, projectService.listAllProjects().size());
	}
}
