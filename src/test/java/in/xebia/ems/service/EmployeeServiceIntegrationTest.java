package in.xebia.ems.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import in.xebia.ems.domain.Employee;
import in.xebia.ems.domain.Role;
import in.xebia.ems.testData.DataGenerator;
import in.xebia.ems.utils.DateUtils;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;


@ContextConfiguration("/applicationContext-test.xml")
public class EmployeeServiceIntegrationTest extends AbstractTransactionalJUnit4SpringContextTests{

	@Resource(name="employeeService")
	private EmployeeService employeeService;
	
	@Resource(name="roleService")
	private RoleService roleService;
	
	@PersistenceContext
	EntityManager entityManager;

	@Before
	public void setUp() {
	}

	@Test
	public void shouldSaveEmployeeWithRole() {
		// Create a Role and Save it
		Role role = DataGenerator.createRole("ROLE_TEST");
		roleService.saveRole(role);
		assertNotNull(roleService.findRoleByRoleName("ROLE_TEST"));

		
		List<Role> roles = new ArrayList<Role>();
		roles.add(role);

		Employee employee = DataGenerator.createEmployee("testEmployee",
				"testPassword", "Jason", "Bourne", "testemail@test.com", true,
				true, DateUtils.toTimeStamp("28-03-2010 12:35:00"),
				DateUtils.toTimeStamp("28-03-1983 12:35:00"), roles);
		
		employeeService.saveEmployee(employee);

		// Find the saved Employee
		Employee savedEmployee = employeeService.findByUsernameAndPassword(
				"testEmployee", "testPassword");
		assertNotNull(savedEmployee);
		assertEquals(savedEmployee.getRoles().get(0).getRoleId(),
				role.getRoleId());
	}
	
	//@Test
	public void shouldDeleteEmployee(){
		Employee employee = DataGenerator.createEmployeeWithDefaultData();
		employeeService.saveEmployee(employee);
		Employee savedEmployee = employeeService.findByUsernameAndPassword(employee.getUsername(), employee.getPassword());
		assertNotNull(savedEmployee);
		employeeService.removeEmployee(savedEmployee);
		Employee deletedEmployee = employeeService.findByUsernameAndPassword(employee.getUsername(), employee.getPassword());
		assertNull(deletedEmployee);
	}
	
	@Test
	public void shouldTestUniqueness(){
		Role role = DataGenerator.createRole("ROLE_TEST");
		roleService.saveRole(role);
		List<Role> roles = new ArrayList<Role>();
		roles.add(role);
		Employee employee = DataGenerator.createEmployeeWithDefaultData();
		employee.setRoles(roles);
		employeeService.saveEmployee(employee);
		Employee employee2 = DataGenerator.createEmployeeWithDefaultData();
		employee2.setRoles(roles);
		assertNull(employeeService.saveEmployee(employee2));
	}
	
	//@Test
	public void shouldListAllEmployees(){
		Employee employee1 = DataGenerator.createEmployee("testEmployee1",
				"testPassword", "Jason", "Bourne", "testemail@test.com", true,
				true, DateUtils.toTimeStamp("28-03-2010 12:35:00"),
				DateUtils.toTimeStamp("28-03-1983 12:35:00"), null);	
		Employee employee2 = DataGenerator.createEmployee("testEmployee2",
				"testPassword", "Jason", "Bourne", "testemail@test.com", true,
				true, DateUtils.toTimeStamp("28-03-2010 12:35:00"),
				DateUtils.toTimeStamp("28-03-1983 12:35:00"), null);
		Employee employee3 = DataGenerator.createEmployee("testEmployee2",
				"testPassword", "Jason", "Bourne", "testemail@test.com", true,
				true, DateUtils.toTimeStamp("28-03-2010 12:35:00"),
				DateUtils.toTimeStamp("28-03-1983 12:35:00"), null);		
		employeeService.saveEmployee(employee1);
		employeeService.saveEmployee(employee2);
		employeeService.saveEmployee(employee3);
		
		//There should be 2 employees, the 3rd employee will not be saved because of duplicate username
		assertEquals(2, employeeService.listAllEmployees().size());
	}
	
}
