package in.xebia.ems.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertEquals;
import in.xebia.ems.domain.Role;
import in.xebia.ems.testData.DataGenerator;

import javax.annotation.Resource;

import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;

@ContextConfiguration("/applicationContext-test.xml")
public class RoleServiceIntegrationTest extends AbstractTransactionalJUnit4SpringContextTests{
	
	@Resource(name="roleService")
	RoleService roleService;
	
	
	@Test
	public void shouldSaveRole(){
		roleService.saveRole(DataGenerator.createRole("ROLE_TEST"));
		Role savedRole = roleService.findRoleByRoleName("ROLE_TEST");
		assertNotNull(savedRole);
	}
	
	@Test
	public void shouldDeleteRole(){
		Role role = DataGenerator.createRoleWithDefaultData();
		roleService.saveRole(role);
		Role savedRole = roleService.findRoleByRoleName(role.getRoleName());
		assertNotNull(savedRole);
		roleService.deleteRole(savedRole);
		Role deleteRole = roleService.findRoleByRoleName(role.getRoleName());
		assertNull(deleteRole);
	}
	
	@Test
	public void shouldReturnAllRoles(){
		roleService.saveRole(DataGenerator.createRole("ROLE_TEST1"));
		roleService.saveRole(DataGenerator.createRole("ROLE_TEST2"));
		roleService.saveRole(DataGenerator.createRole("ROLE_TEST3"));
		assertEquals(roleService.listAllRoles().size(), 3);
	}
	
}
