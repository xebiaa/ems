-- phpMyAdmin SQL Dump
-- version 3.2.2.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 23, 2010 at 03:44 PM
-- Server version: 5.1.37
-- PHP Version: 5.2.10-2ubuntu6.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ems`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE IF NOT EXISTS `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_425ae3c4` (`group_id`),
  KEY `auth_group_permissions_1e014c8f` (`permission_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=175 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_message`
--

CREATE TABLE IF NOT EXISTS `auth_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_message_403f60f` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE IF NOT EXISTS `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_1bb8f392` (`content_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=79 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE IF NOT EXISTS `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=150 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE IF NOT EXISTS `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_403f60f` (`user_id`),
  KEY `auth_user_groups_425ae3c4` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_403f60f` (`user_id`),
  KEY `auth_user_user_permissions_1e014c8f` (`permission_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE IF NOT EXISTS `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_403f60f` (`user_id`),
  KEY `django_admin_log_1bb8f392` (`content_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=122 ;

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE IF NOT EXISTS `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE IF NOT EXISTS `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employee_employeeofficialdetail`
--

CREATE TABLE IF NOT EXISTS `employee_employeeofficialdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `emp_id` varchar(256) NOT NULL,
  `date_of_joining` date NOT NULL,
  `date_of_confirmation` date DEFAULT NULL,
  `designation` varchar(256) DEFAULT NULL,
  `mentor_id` int(11) DEFAULT NULL,
  `manager_id` int(11) DEFAULT NULL,
  `cur_projects` varchar(256) DEFAULT NULL,
  `experience` varchar(256) DEFAULT NULL,
  `previous_org` varchar(256) DEFAULT NULL,
  `previous_org_add` varchar(256) DEFAULT NULL,
  `skillset` varchar(256) DEFAULT NULL,
  `last_updated` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `employee_employeeofficialdetail_3159ad59` (`mentor_id`),
  KEY `employee_employeeofficialdetail_501a2222` (`manager_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=93 ;

-- --------------------------------------------------------

--
-- Table structure for table `employee_employeepersonaldetail`
--

CREATE TABLE IF NOT EXISTS `employee_employeepersonaldetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `marital_status` varchar(1) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `marriage_date` date DEFAULT NULL,
  `phone_number` varchar(12) DEFAULT NULL,
  `cell_number` varchar(12) DEFAULT NULL,
  `current_address` varchar(256) DEFAULT NULL,
  `permanent_address` varchar(256) DEFAULT NULL,
  `imergancy_contact` varchar(256) DEFAULT NULL,
  `imergancy_contact_num` varchar(12) DEFAULT NULL,
  `passport_number` varchar(256) DEFAULT NULL,
  `passport_authority` varchar(256) DEFAULT NULL,
  `passport_issue_date` date DEFAULT NULL,
  `passport_expire_date` date DEFAULT NULL,
  `pan_number` varchar(256) DEFAULT NULL,
  `license_number` varchar(256) DEFAULT NULL,
  `blood_gp` varchar(64) DEFAULT NULL,
  `spouse_name` varchar(256) DEFAULT NULL,
  `spouse_dob` date DEFAULT NULL,
  `father_name` varchar(256) DEFAULT NULL,
  `mother_name` varchar(256) DEFAULT NULL,
  `personal_email` varchar(64) DEFAULT NULL,
  `skype_id` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=96 ;

-- --------------------------------------------------------

--
-- Table structure for table `leave_accruallog`
--

CREATE TABLE IF NOT EXISTS `leave_accruallog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `log_date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `leave_accruallog_fbfc09f1` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `leave_employeeleave`
--

CREATE TABLE IF NOT EXISTS `leave_employeeleave` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `lt_id` int(11) NOT NULL,
  `balance` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `leave_employeeleave_fbfc09f1` (`user_id`),
  KEY `leave_employeeleave_f579c646` (`lt_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1780 ;

-- --------------------------------------------------------

--
-- Table structure for table `leave_generalleave`
--

CREATE TABLE IF NOT EXISTS `leave_generalleave` (
  `leave_ptr_id` int(11) NOT NULL,
  `lt_id` int(11) NOT NULL,
  PRIMARY KEY (`leave_ptr_id`),
  KEY `leave_generalleave_f579c646` (`lt_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `leave_leave`
--

CREATE TABLE IF NOT EXISTS `leave_leave` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `lfrom` date NOT NULL,
  `lto` date NOT NULL,
  `fsegment_id` int(11) NOT NULL,
  `tsegment_id` int(11) NOT NULL,
  `dates` longtext NOT NULL,
  `duration` double NOT NULL,
  `remark` longtext NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `applied_date` date NOT NULL,
  `admin_remark` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `leave_leave_fbfc09f1` (`user_id`),
  KEY `leave_leave_f2fedacb` (`fsegment_id`),
  KEY `leave_leave_e5fa868f` (`tsegment_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=683 ;

-- --------------------------------------------------------

--
-- Table structure for table `leave_leavesegment`
--

CREATE TABLE IF NOT EXISTS `leave_leavesegment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `segment` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `leave_leavetype`
--

CREATE TABLE IF NOT EXISTS `leave_leavetype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `leave_offsiteleave`
--

CREATE TABLE IF NOT EXISTS `leave_offsiteleave` (
  `leave_ptr_id` int(11) NOT NULL,
  `lt_id` int(11) NOT NULL,
  PRIMARY KEY (`leave_ptr_id`),
  KEY `leave_offsiteleave_f579c646` (`lt_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `leave_offsitetype`
--

CREATE TABLE IF NOT EXISTS `leave_offsitetype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `leave_publicholidays`
--

CREATE TABLE IF NOT EXISTS `leave_publicholidays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `project_employeeproject`
--

CREATE TABLE IF NOT EXISTS `project_employeeproject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `joining_date` date DEFAULT NULL,
  `releasing_date` date DEFAULT NULL,
  `percent_allocated` varchar(8) DEFAULT NULL,
  `billable` int(1) NOT NULL DEFAULT '1',
  `remarks` varchar(256) DEFAULT NULL,
  `wish_list` varchar(256) DEFAULT NULL,
  `tent_allocation` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_employeeproject_359f2ab4` (`employee_id`),
  KEY `project_employeeproject_499df97c` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=139 ;

-- --------------------------------------------------------

--
-- Table structure for table `project_project`
--

CREATE TABLE IF NOT EXISTS `project_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `client` varchar(64) NOT NULL,
  `phone_number` varchar(64) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `technology` varchar(64) DEFAULT NULL,
  `date_of_starting` date NOT NULL,
  `date_of_closing` date DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

-- --------------------------------------------------------

--
-- Table structure for table `project_projectcontact`
--

CREATE TABLE IF NOT EXISTS `project_projectcontact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `designation` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `phone_number` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_projectcontact_499df97c` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `reports_wishlist`
--

CREATE TABLE IF NOT EXISTS `reports_wishlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `added_by_id` int(11) NOT NULL,
  `story` varchar(500) NOT NULL,
  `category` varchar(64) NOT NULL,
  `status` varchar(64) NOT NULL,
  `dev_comment` varchar(500) NOT NULL,
  `ts` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `reports_wishlist_72f634e2` (`added_by_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=45 ;
