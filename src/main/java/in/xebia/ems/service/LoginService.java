package in.xebia.ems.service;


public interface LoginService {
	
	public boolean isAuthenticUser(String username, String password);
	
}
