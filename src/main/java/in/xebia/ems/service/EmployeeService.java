package in.xebia.ems.service;

import in.xebia.ems.domain.Employee;

import java.util.List;

/**
 *
 * @author rocky
 */
public interface EmployeeService {

	Employee saveEmployee(Employee employee);
	
	Employee findByUsernameAndPassword(String username, String password);
	
	Employee findByUsername(String username);
	
	void removeEmployee(Employee employee);
	
	List<Employee> listAllEmployees();

	boolean isUniqueUsername(String username);
	
	List<Employee> listAllActiveEmployees();
}
