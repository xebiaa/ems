package in.xebia.ems.service;

import in.xebia.ems.domain.Project;

import java.util.List;

/**
 *
 * @author tushar
 */
public interface ProjectService {

	Project saveProject(Project project);
	
	void removeProject(Project project);
	
	List<Project> listAllProjects();
	
	Project findProjectByName(String projectName);

}
