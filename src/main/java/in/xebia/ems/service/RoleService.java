package in.xebia.ems.service;

import in.xebia.ems.domain.Role;

import java.util.List;

public interface RoleService {
	Role saveRole(Role role);
	void deleteRole(Role role);
	Role findRoleByRoleName(String roleName);
	List<Role> listAllRoles();
	List<String> roleNameList();
}
