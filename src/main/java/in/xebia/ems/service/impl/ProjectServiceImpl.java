package in.xebia.ems.service.impl;

import in.xebia.ems.dao.impl.ProjectDAOHibernate;
import in.xebia.ems.domain.Project;
import in.xebia.ems.service.ProjectService;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author rocky
 */
@Service("projectService")
@Transactional
public class ProjectServiceImpl implements ProjectService {

	@Resource
	private ProjectDAOHibernate projectDAOHibernate;

	@Override
	public Project saveProject(Project project) {
			return projectDAOHibernate.persist(project);
	}

	@Override
	public List<Project> listAllProjects() {
		return projectDAOHibernate.findAll();
	}

	@Override
	public void removeProject(Project project) {
		projectDAOHibernate.deleteById(project.getId());
	}

	@Override
	public Project findProjectByName(String projectName) {
		List<Project> project = projectDAOHibernate.findByProjectName(projectName);
		if(project!=null && project.size()==1)
			return project.get(0);
		else
			return null;
	}
	




}
