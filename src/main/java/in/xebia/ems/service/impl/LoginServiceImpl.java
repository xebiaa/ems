package in.xebia.ems.service.impl;

import in.xebia.ems.dao.impl.EmployeeDAOHibernate;
import in.xebia.ems.domain.Employee;
import in.xebia.ems.service.LoginService;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

@Service("loginService")
public class LoginServiceImpl implements LoginService {

	@Resource
	EmployeeDAOHibernate employeeDAOHibernate;
	
	@Override
	public boolean isAuthenticUser(String username, String password) {
		List<Employee> employees = employeeDAOHibernate.findByUsernameAndPassword(username, password);
		if(employees!=null && employees.size()==1)
			return true;
		else
			return false;
	}

}
