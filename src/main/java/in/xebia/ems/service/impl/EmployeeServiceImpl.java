package in.xebia.ems.service.impl;

import in.xebia.ems.dao.impl.EmployeeDAOHibernate;
import in.xebia.ems.dao.impl.RoleDAOHibernate;
import in.xebia.ems.domain.Employee;
import in.xebia.ems.domain.Role;
import in.xebia.ems.service.EmployeeService;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author rocky
 * @author Mukesh Shah
 */
@Service("employeeService")
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	@Resource
	private EmployeeDAOHibernate employeeDAOHibernate;
	
	@Resource
	private RoleDAOHibernate roleDAOHibernate;

	@Override
	public Employee saveEmployee(Employee employee) {
		if(isUniqueUsername(employee.getUsername())) {
			//setting role of employee
			for(Role role : employee.getRoles()){
				List<Role> roles = roleDAOHibernate.findByFields("roleName", role.getRoleName());
				if(roles != null && roles.size() > 0) {
					employee.setRoles(roles);
				}
			}
			return employeeDAOHibernate.persist(employee);
		}
		else 
			return null;
	}
	
	@Override
	public Employee findByUsernameAndPassword(String username, String password) {
		List<Employee> employees = employeeDAOHibernate.findByUsernameAndPassword(username, password);
		if(employees!=null && employees.size()==1)
			return employees.get(0);
		else
			return null;

	}

	@Override
	public void removeEmployee(Employee employee) {
		employeeDAOHibernate.deleteById(employee.getUserId());
	}

	@Override
	public List<Employee> listAllEmployees() {
		return employeeDAOHibernate.findAll();
	}

	@Override
	public boolean isUniqueUsername(String username) {
		List<Employee> employees = employeeDAOHibernate.findByFields("username", username);
		if(employees.size()>0)
			return false;
		return true;
	}

	@Override
	public Employee findByUsername(String username) {
		List<Employee> employees = employeeDAOHibernate.findByFields("username", username);
		if(employees!=null && employees.size()==1)
			return employees.get(0);
		else
			return null;
	}
	
	@Override
	public List<Employee> listAllActiveEmployees() {
		List<Employee> employees = employeeDAOHibernate.listAllActiveEmployees();
		return employees;
		
	}

}
