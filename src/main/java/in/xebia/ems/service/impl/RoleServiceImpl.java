package in.xebia.ems.service.impl;

import in.xebia.ems.dao.impl.RoleDAOHibernate;
import in.xebia.ems.domain.Role;
import in.xebia.ems.service.RoleService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("roleService")
public class RoleServiceImpl implements RoleService {
	
	@Resource
	private RoleDAOHibernate roleDAOHibernate;

	@Override
	@Transactional
	public Role saveRole(Role role) {
		return roleDAOHibernate.persist(role);
	}
	
	@Override
	@Transactional	
	public void deleteRole(Role role){
		roleDAOHibernate.deleteById(role.getRoleId());
	}

	@Override
	public Role findRoleByRoleName(String roleName) {
		List<Role> roles = roleDAOHibernate.findByFields("roleName", roleName);
		if(roles!=null && roles.size()==1)
			return roles.get(0);
		else
			return null;
	}

	@Override
	public List<Role> listAllRoles() {
		return roleDAOHibernate.findAll();
	}

	@Override
	public List<String> roleNameList() {
		List<Role> roles = roleDAOHibernate.findAll();
		List<String> roleNames = new ArrayList<String>();
		for(Role role : roles){
			roleNames.add(role.getRoleName());
		}
		return roleNames;
	}
	
	
}
