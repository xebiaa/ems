package in.xebia.ems;

import in.xebia.ems.webapp.HomePage;
import in.xebia.ems.webapp.LoginPage;
import org.apache.wicket.Page;
import org.apache.wicket.authentication.AuthenticatedWebApplication;
import org.apache.wicket.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.apache.wicket.util.lang.PackageName;

/**
 *
 * @author rocky
 */
public class EMSApplication extends AuthenticatedWebApplication {

	boolean isInitialized = false;

	public EMSApplication() {
		super();
		mount("employee", PackageName.forClass(in.xebia.ems.webapp.employee.EmployeeAdd.class));
	}
	
	@Override
	protected void init() {
		if (!isInitialized) {
			super.init();
			setListeners();
			isInitialized = true;
		}
	}

	private void setListeners() {
		addComponentInstantiationListener(new SpringComponentInjector(this));
	}

	@Override
	protected Class<? extends WebPage> getSignInPageClass() {
		return LoginPage.class;
	}

	@Override
	protected Class<? extends AuthenticatedWebSession> getWebSessionClass() {
		return MyAuthenticatedWebSession.class;
	}

	@Override
	public Class<? extends Page> getHomePage() {
		return HomePage.class;
	}
	
}
