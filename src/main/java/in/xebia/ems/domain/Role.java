package in.xebia.ems.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Role implements Serializable{

	@Id
	@GeneratedValue
	private Long roleId;

	@ManyToMany
	@JoinTable(name="user_role", joinColumns={@JoinColumn(name="roleId", referencedColumnName="roleId")},
			inverseJoinColumns={@JoinColumn(name="userId", referencedColumnName="userId")})	
	private List<Employee> employees;	
	
	private String roleName;

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public List<Employee> getEmployees() {
		return employees;
	}
	
	public String toString(){
		return roleName;
	}
}
