package in.xebia.ems.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;


@Entity
public class Employee implements Serializable {

	@Id
	@GeneratedValue
	private Long userId;

	@Column(nullable = false)
	private String username;

	@Column(nullable = false)
	private String password;

	@Column(nullable = false)
	private String firstName;

	@Column(nullable = false)
	private String lastName;

	@Column(nullable = false)
	private String email;

	@Column(nullable = false)
	private Boolean staff;

	@Column(nullable = false)
	private Boolean active;

	@Column(nullable = true)
	private Date lastLogin;

	@Column(nullable = false)
	private Date dateJoined;

	@ManyToMany(fetch = FetchType.EAGER, cascade={CascadeType.PERSIST})
	@JoinTable(name = "user_role", joinColumns = { @JoinColumn(name = "userId", referencedColumnName = "userId") }, inverseJoinColumns = { @JoinColumn(name = "roleId", referencedColumnName = "roleId") })
	private List<Role> roles;

	public Long getUserId() {
		return userId;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public String[] getRolesAsString() {
		String[] rolesArray = new String[roles.size()];
		for (int i = 0; i < roles.size(); i++) {
			rolesArray[i] = roles.get(i).getRoleName();
		}
		return rolesArray;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getActive() {
		return active;
	}

	public Boolean getStaff() {
		return staff;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public void setStaff(Boolean staff) {
		this.staff = staff;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public Date getDateJoined() {
		return dateJoined;
	}

	public void setDateJoined(Date dateJoined) {
		this.dateJoined = dateJoined;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
