package in.xebia.ems.webapp.employee;

import in.xebia.ems.domain.Employee;
import in.xebia.ems.service.EmployeeService;
import in.xebia.ems.webapp.EMSBaseWebPage;

import org.apache.wicket.PageParameters;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.spring.injection.annot.SpringBean;

public class EmployeeDetails extends EMSBaseWebPage {

	@SpringBean(name = "employeeService")
	private EmployeeService employeeService;
	
	public EmployeeDetails(PageParameters pageParams){
		super();
		String userName = pageParams.getString("uname");
		Employee employee = employeeService.findByUsername(userName);
		//add(new Label("messageED", "Employee Details"));
		add(new Label("emplayeeName", employee.getFirstName()+ " " + employee.getLastName()));
		add(new Label("firstName", employee.getFirstName()));
		add(new Label("lastName", employee.getLastName()));
		add(new Label("userName", employee.getUsername()));
		add(new Label("password", employee.getPassword()));
		add(new Label("userEmial", employee.getEmail()));
		add(new Label("userJoined", employee.getDateJoined().toString()));
		add(new Label("userRole", employee.getRoles().toString()));
		String isActive = employee.getActive().toString().toUpperCase();
		add(new Label("userIsActive", isActive));
	}
}
