package in.xebia.ems.webapp.employee;

import org.apache.wicket.markup.html.basic.Label;

import in.xebia.ems.webapp.EMSBaseWebPage;

public class ChangePassword extends EMSBaseWebPage {

	public ChangePassword(){
		super();
		add(new Label("messageCP", "Change Password"));
	}
}
