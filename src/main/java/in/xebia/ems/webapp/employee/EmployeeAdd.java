package in.xebia.ems.webapp.employee;

import in.xebia.ems.domain.Employee;
import in.xebia.ems.service.EmployeeService;
import in.xebia.ems.service.RoleService;
import in.xebia.ems.utils.SelectOption;
import in.xebia.ems.webapp.EMSBaseWebPage;
import in.xebia.ems.webapp.utils.EmployeeAddForm;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.value.ValueMap;
import org.apache.wicket.validation.validator.EmailAddressValidator;
import org.apache.wicket.validation.validator.StringValidator;


public class EmployeeAdd extends EMSBaseWebPage {
	
	private EmployeeAddForm employeeAddForm;
	private TextField usernameTextField;
	private TextField passwordTextField;
	private TextField eIdTextField;
	private TextField joiningDateTextField;
	private TextField fNameTextField;
	private TextField lNameTextField;
	private TextField emailTextField;
	private CheckBox isActiveCheckBox;
	private DropDownChoice roleDropDown;
	private Button submitButton;
	private DropDownChoice mentorsDropDown;
	private DropDownChoice managersDropDown;
	
	@SpringBean(name="roleService")
	RoleService roleService;
	
	@SpringBean(name="employeeService")
	private EmployeeService employeeService;
	
	private final ValueMap properties = new ValueMap();
	
	public EmployeeAdd() {
		super();
		createForm();
		add(employeeAddForm);
	}
	
	
	private void createForm(){
		
		employeeAddForm = new EmployeeAddForm("employeeAddForm"){
			public void extractSubmittedValues() {
				username = usernameTextField.getInput();
				password = passwordTextField.getInput();
				eId = eIdTextField.getInput();
				joiningDate = joiningDateTextField.getInput();
				fName = fNameTextField.getInput();
				lName = lNameTextField.getInput();
				email = emailTextField.getInput();
				isActive = isActiveCheckBox.getConvertedInput().toString();
				List<String> rolesList = new ArrayList<String>();
				rolesList.add(roleDropDown.getConvertedInput().toString());
				roles = rolesList;
				selectedMentor = (SelectOption)mentorsDropDown.getConvertedInput();
				selectedManager = (SelectOption)managersDropDown.getConvertedInput();
			}
		};
		
		usernameTextField = new TextField("Username", new PropertyModel(properties, "Username"));
		usernameTextField.setRequired(true);
		usernameTextField.add(StringValidator.maximumLength(15));
		
		passwordTextField = new PasswordTextField("Password", new PropertyModel(properties, "Password"));
		passwordTextField.setRequired(true);
		passwordTextField.add(StringValidator.maximumLength(15));
		
		eIdTextField = new TextField("Employee ID", new PropertyModel(properties, "Employee ID"));
		eIdTextField.setRequired(true);
		
		joiningDateTextField = new TextField("Date of Joining", new PropertyModel(properties, "Date of Joining"));
		joiningDateTextField.setRequired(true);
		
		fNameTextField = new TextField("First Name", new PropertyModel(properties, "First Name"));
		fNameTextField.setRequired(true);
		fNameTextField.add(StringValidator.maximumLength(15));
		
		lNameTextField = new TextField("Last Name", new PropertyModel(properties, "Last Name"));
		lNameTextField.setRequired(true);
		fNameTextField.add(StringValidator.maximumLength(15));
		
		emailTextField = new TextField("Email", new PropertyModel(properties, "Email"));
		emailTextField.setRequired(true);
		emailTextField.add(EmailAddressValidator.getInstance());

		isActiveCheckBox = new CheckBox("isActiveCheckBox", new PropertyModel(properties, "isActiveCheckBox"));
		roleDropDown = new DropDownChoice("Group", new PropertyModel(properties, "Group"), roleService.roleNameList());
		roleDropDown.setRequired(true);
		
		List<Employee> employees = employeeService.listAllActiveEmployees();
		List<SelectOption> options = new ArrayList<SelectOption>();
		for(Employee employee : employees){
			options.add(new SelectOption(employee.getFirstName() + " " + employee.getLastName(), String.valueOf(employee.getUserId())));
		}
		ChoiceRenderer choiceRenderer = new ChoiceRenderer("key", "value");
		
		mentorsDropDown = new DropDownChoice("Mentor", new PropertyModel(properties, "Mentor"), options, choiceRenderer);
		mentorsDropDown.setRequired(true);
		
		managersDropDown = new DropDownChoice("Manager", new PropertyModel(properties, "Manager"), options, choiceRenderer);
		managersDropDown.setRequired(true);
		
		submitButton = new Button("submitButton");
		
		employeeAddForm.add(usernameTextField);
		employeeAddForm.add(passwordTextField);
		employeeAddForm.add(eIdTextField);
		employeeAddForm.add(joiningDateTextField);
		employeeAddForm.add(fNameTextField);
		employeeAddForm.add(lNameTextField);
		employeeAddForm.add(emailTextField);
		employeeAddForm.add(isActiveCheckBox);
		employeeAddForm.add(roleDropDown);
		employeeAddForm.add(mentorsDropDown);
		employeeAddForm.add(managersDropDown);
		employeeAddForm.add(submitButton);
	}
	
}
