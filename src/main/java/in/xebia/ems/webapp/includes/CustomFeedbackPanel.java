package in.xebia.ems.webapp.includes;

import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;

public class CustomFeedbackPanel extends Panel{

	public CustomFeedbackPanel(String id) {
		super(id);
		
		add(new FeedbackPanel("feedBack"){
			public boolean isVisible() {
				return anyErrorMessage();
			}
		});
	}
}
