package in.xebia.ems.webapp.includes;

import in.xebia.ems.MyAuthenticatedWebSession;
import in.xebia.ems.domain.Employee;
import in.xebia.ems.webapp.LoginPage;
import in.xebia.ems.webapp.employee.ChangePassword;
import in.xebia.ems.webapp.employee.EmployeeDetails;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;

public class HeaderPanel extends Panel {
	private Link employeeDetails;
	private Link changePassword;
	private Link logout;

	public HeaderPanel(String id) {
		super(id);

		employeeDetails = new Link("employeeDetails") {
			@Override
			public void onClick() {
				setResponsePage(EmployeeDetails.class);
			}
		};
		Employee employee = MyAuthenticatedWebSession.get().getUser();
		String employeeName = "Default";
		if (employee != null) {
			employeeName = employee.getFirstName() + " "
					+ employee.getLastName();
		}
		employeeDetails.add(new Label("employeeName", employeeName));
		add(employeeDetails);

		changePassword = new Link("changePassword") {
			@Override
			public void onClick() {
				setResponsePage(ChangePassword.class);
			}
		};
		add(changePassword);
		logout = new Link("logout") {
			@Override
			public void onClick() {
				MyAuthenticatedWebSession.get().signOut();
				MyAuthenticatedWebSession.get().invalidate();
				setResponsePage(LoginPage.class);
			}
		};
		add(logout);
		
	}

}
