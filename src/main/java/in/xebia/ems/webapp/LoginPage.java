package in.xebia.ems.webapp;

import org.apache.wicket.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.util.value.ValueMap;

/**
 *
 * @author rocky
 */
public class LoginPage extends WebPage {
	
	private Form loginForm;
	private TextField usernameTextField;
	private PasswordTextField passwordTextField;
	private Button loginButton;
	
	private final ValueMap properties = new ValueMap();
	public LoginPage() {
		loginForm = new Form("loginForm"){
			protected void onSubmit() {
				String username = usernameTextField.getInput();
				String password = passwordTextField.getInput();
				if(AuthenticatedWebSession.get().signIn(username, password)){
					signInSucceed();
				}else{
					signInFailed();
				}
			}
		};
		usernameTextField = new TextField("usernameTextField", new PropertyModel(properties, "usernameTextField"));
		passwordTextField = new PasswordTextField("passwordTextField", new PropertyModel(properties, "passwordTextField"));
		loginButton = new Button("loginButton", new PropertyModel(properties, "loginButton"));
		loginForm.add(usernameTextField);
		loginForm.add(passwordTextField);
		loginForm.add(loginButton);
		add(loginForm);
	}
	
	private void signInSucceed(){
		setResponsePage(HomePage.class);
	}
	
	private void signInFailed(){
		setResponsePage(LoginPage.class);
	}
	
}
