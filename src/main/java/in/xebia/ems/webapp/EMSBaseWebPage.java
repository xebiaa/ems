package in.xebia.ems.webapp;

import in.xebia.ems.webapp.includes.CustomFeedbackPanel;
import in.xebia.ems.webapp.includes.FooterPanel;
import in.xebia.ems.webapp.includes.HeaderPanel;
import in.xebia.ems.webapp.includes.IncludesPanel;
import in.xebia.ems.webapp.includes.TabsPanel;

import org.apache.wicket.markup.html.WebPage;

public class EMSBaseWebPage extends WebPage {
	
	//private Link logOutLink;
	public EMSBaseWebPage() {
		add(new IncludesPanel("includes"));
		add(new HeaderPanel("header"));
		add(new CustomFeedbackPanel("feedback"));
		add(new TabsPanel("tabs"));
		add(new FooterPanel("footer"));
	}
}
