
package in.xebia.ems.webapp;

import in.xebia.ems.domain.Project;
import in.xebia.ems.service.ProjectService;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.value.ValueMap;

/**
 *
 * @author tushar
 */
public class ProjectPage extends WebPage {
	
	private Form projectForm;
	private TextField projectNameTextField;
	private TextField technologyTextField;
	private TextField clientNameField;
	private TextField startDateField;
	private TextField endDateField;
	private TextArea descriptionTextField;
	private TextArea clientAddressTextField;
	private Button createButton;
	@SpringBean
	private ProjectService projectService;
	private final ValueMap properties = new ValueMap();
	public ProjectPage() {
		projectForm = new Form("projectForm"){
			protected void onSubmit() {
				Project project = new Project();
				project.setProjectName(projectNameTextField.getInput());
				project.setTechnology(technologyTextField.getInput());
				project.setClientAddress(clientAddressTextField.getInput());
				project.setClientName(clientNameField.getInput());
				project.setCloseDate(endDateField.getInput());
				project.setDescription(descriptionTextField.getInput());
				project.setStartDate(startDateField.getInput());
				projectService.saveProject(project);
			}
		};
		projectNameTextField = new TextField("projectNameTextField", new PropertyModel(properties, "projectNameTextField"));
		technologyTextField = new TextField("technologyTextField", new PropertyModel(properties,"technologyTextField"));
		clientNameField = new TextField("clientNameField", new PropertyModel(properties,"clientNameField"));
		startDateField = new TextField("startDateField", new PropertyModel(properties,"startDateField"));
		endDateField = new TextField("endDateField", new PropertyModel(properties,"endDateField"));
		descriptionTextField = new TextArea("descriptionTextField", new PropertyModel(properties,"descriptionTextField"));
		clientAddressTextField = new TextArea("clientAddressTextField", new PropertyModel(properties,"clientAddressTextField"));
		createButton = new Button("createButton", new PropertyModel(properties, "createButton"));
		
		projectForm.add(projectNameTextField);
		projectForm.add(technologyTextField);
		projectForm.add(clientNameField);
		projectForm.add(startDateField);
		projectForm.add(endDateField);
		projectForm.add(descriptionTextField);
		projectForm.add(clientAddressTextField);
		projectForm.add(createButton);
		add(projectForm);
	}
	
	
}
