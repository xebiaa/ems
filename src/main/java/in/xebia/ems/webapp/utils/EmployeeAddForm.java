package in.xebia.ems.webapp.utils;

import in.xebia.ems.domain.Employee;
import in.xebia.ems.domain.Role;
import in.xebia.ems.service.EmployeeService;
import in.xebia.ems.utils.SelectOption;
import in.xebia.ems.webapp.HomePage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.spring.injection.annot.SpringBean;

public abstract class  EmployeeAddForm extends Form{
	
	protected String username;
	protected String password;
	protected String eId;
	protected String joiningDate;
	protected String fName;
	protected String lName;
	protected String email;
	protected String isActive;
	protected List<String> roles;
	protected SelectOption selectedMentor;
	protected SelectOption selectedManager;

	
	private EmployeeAddFormValidator employeeAddFormValidator;
	
	@SpringBean(name="employeeService")
	private EmployeeService employeeService;
	
	protected void onSubmit() {
		extractSubmittedValues();
		if(employeeAddFormValidator.validate()){
			Employee employee = createEmployee();
			saveEmployee(employee);
			setResponsePage(HomePage.class, null);

		}else{
			
		}
	}

	private Employee createEmployee(){
		Employee employee = new Employee();
		employee.setUsername(username);
		employee.setPassword(password);
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Date dateOfJoining;
		try {
			dateOfJoining = dateFormat.parse(joiningDate);
			employee.setDateJoined(dateOfJoining);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		employee.setFirstName(fName);
		employee.setLastName(lName);
		employee.setEmail(email);
		employee.setActive(new Boolean(isActive));
		//todo: do we need to put this field on UI
		employee.setStaff(true);
		
		List<Role> selectedRoles = new ArrayList<Role>();
		for(String roleName : roles){
			Role role = new Role();
			role.setRoleName(roleName);
			selectedRoles.add(role);
		}
		employee.setRoles(selectedRoles);
		
		return employee;
	}
	
	private void saveEmployee(Employee employee){
		employeeService.saveEmployee(employee);
	}
	
	public EmployeeAddForm(String id) {
		super(id);
		employeeAddFormValidator = new EmployeeAddFormValidator(this, employeeService);
	}
	
	public abstract void extractSubmittedValues();

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String geteId() {
		return eId;
	}

	public String getJoiningDate() {
		return joiningDate;
	}

	public String getfName() {
		return fName;
	}

	public String getlName() {
		return lName;
	}

	public String getEmail() {
		return email;
	}

	public String getIsActive() {
		return isActive;
	}

	public List<String> getRoles() {
		return roles;
	}
	
	public SelectOption getSelectedMentor() {
		return selectedMentor;
	}

	public SelectOption getSelectedManager() {
		return selectedManager;
	}
	
}
