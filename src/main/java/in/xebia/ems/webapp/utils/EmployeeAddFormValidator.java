package in.xebia.ems.webapp.utils;

import java.io.Serializable;

import in.xebia.ems.service.EmployeeService;

import org.apache.wicket.spring.injection.annot.SpringBean;


public class EmployeeAddFormValidator implements Serializable{

	private static final long serialVersionUID = 1L;
	private EmployeeAddForm employeeAddForm;
	private String validationMessage;
	private EmployeeService employeeService;
	
	public EmployeeAddFormValidator(EmployeeAddForm 
			employeeAddForm, EmployeeService employeeService) {
		this.employeeAddForm = employeeAddForm;
		this.employeeService = employeeService;
	}
	
	public boolean validate(){
		boolean validationResult = true;
		
		//isUsername unique		
		if(employeeService.findByUsername(employeeAddForm.getUsername())!=null){
			validationMessage = "Username not unique";
			validationResult = false;
		}

		return validationResult;
	}
	
}
