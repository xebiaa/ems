package in.xebia.ems.webapp;

import in.xebia.ems.domain.Employee;
import in.xebia.ems.service.EmployeeService;
import in.xebia.ems.utils.Constants;
import in.xebia.ems.webapp.employee.EmployeeAdd;

import java.util.List;

import org.apache.wicket.authentication.AuthenticatedWebSession;
import org.apache.wicket.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.spring.injection.annot.SpringBean;

/**
 *
 * @author rocky
 * @author mukesh
 */
@AuthorizeInstantiation({"ROLE_ADMIN", "ROLE_USER"})
public class HomePage extends EMSBaseWithHeaderAndFooter {

	@SpringBean(name = "employeeService")
	private EmployeeService employeeService;
	
	public HomePage() {
		add(new Label("message", "User List"));
		
		add(new BookmarkablePageLink("addEmployeePage", EmployeeAdd.class, null){
			public boolean isVisible() {
				AuthenticatedWebSession session = AuthenticatedWebSession.get();
				return session.getRoles().hasRole(Constants.ROLE_ADMIN);
			}
		});
		
		List<Employee> employeesList = employeeService.listAllEmployees();
		ListView<String> listView = new ListView("listview", employeesList) {
		    protected void populateItem(ListItem item) {
		    	Employee employee = (Employee) item.getModelObject();
		    	item.add(new ExternalLink("userName", "/ems/employee/EmployeeDetails?uname=" + employee.getUsername(), employee.getUsername()));
		        item.add(new Label("firstName", employee.getFirstName()));
		        item.add(new Label("lastName",  employee.getLastName()));
		        item.add(new Label("email",     employee.getEmail()));
		        item.add(new Label("isActive", employee.getActive().toString()));
		    }
		};
		add(listView);
	}
	
}
