package in.xebia.ems.webapp;

import in.xebia.ems.webapp.includes.FooterPanel;
import in.xebia.ems.webapp.includes.HeaderPanel;
import in.xebia.ems.webapp.includes.IncludesPanel;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.link.Link;

/**
 * 
 * @author Mukesh Shah
 *
 */
public class EMSBaseWithHeaderAndFooter extends WebPage{
	
	public EMSBaseWithHeaderAndFooter(){
		add(new IncludesPanel("includes"));
		add(new HeaderPanel("header"));
		add(new FooterPanel("footer"));
	}
}
