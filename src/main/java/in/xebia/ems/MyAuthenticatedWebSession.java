package in.xebia.ems;

import in.xebia.ems.domain.Employee;
import in.xebia.ems.service.EmployeeService;

import org.apache.log4j.Logger;
import org.apache.wicket.Request;
import org.apache.wicket.Session;
import org.apache.wicket.authentication.AuthenticatedWebSession;
import org.apache.wicket.authorization.strategies.role.Roles;
import org.apache.wicket.injection.web.InjectorHolder;
import org.apache.wicket.spring.injection.annot.SpringBean;

public class MyAuthenticatedWebSession extends AuthenticatedWebSession {

	private static final Logger logger = Logger.getLogger(MyAuthenticatedWebSession.class);
	
	@SpringBean(name="employeeService")
	private EmployeeService employeeService;
	
	private Employee employee;

	public MyAuthenticatedWebSession(Request request) {
		super(request);
		injectDependencies();
	}

	private void injectDependencies() {
		InjectorHolder.getInjector().inject(this);
	}
	
	public static MyAuthenticatedWebSession get() {
		return (MyAuthenticatedWebSession) Session.get();
	}
	
	public synchronized Employee getUser(){
		return employee;
	}

	@Override
	public boolean authenticate(String username, String password) {
		employee = employeeService.findByUsernameAndPassword(username, password);
		if(employee!=null)
			return true;
		return false;
		
	}

	@Override
	public Roles getRoles() {
		if(isSignedIn()){
			return new Roles(employee.getRolesAsString());
		}
		return null;

	}
}
