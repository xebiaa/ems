package in.xebia.ems.utils;

public class Constants {
	
	/* Roles for ems application */
	public static final String ROLE_ADMIN = "ROLE_ADMIN";
	public static final String ROLE_USER = "ROLE_USER";

}
