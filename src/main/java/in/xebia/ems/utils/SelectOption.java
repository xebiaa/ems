package in.xebia.ems.utils;

import java.io.Serializable;

/**
 * 
 * @author Mukesh Shah
 */
public class SelectOption implements Serializable{

	private String key;
	private String value;
	
	public SelectOption(String key, String value) {
		this.value = value;
		this.key = key;
	}
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	

	

}
