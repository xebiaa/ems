package in.xebia.ems.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
	
	private static final String DATEFORMAT = "dd-MM-yyyy";
	
	private static final String TIMESTAMP = "dd-MM-yyyy hh:mm:ss";
	
	private static SimpleDateFormat dateFormatter = new SimpleDateFormat(DATEFORMAT);
	
	private static SimpleDateFormat timeStampFormatter = new SimpleDateFormat(TIMESTAMP);
	
	public static Date toDate(String dateString){
		try{
			return dateFormatter.parse(dateString);
		}catch(ParseException parseException){
			System.out.println("Exception in parsing the date : "+dateString);
			return null;
		}
	}
	
	public static Date toTimeStamp(String dateString){
		try {
			return timeStampFormatter.parse(dateString);
		} catch (ParseException e) {
			System.out.println("Exception in parsing the timestamp : "+dateString);
			return null;
		}
	}
}
