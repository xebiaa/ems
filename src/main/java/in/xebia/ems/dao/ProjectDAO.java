package in.xebia.ems.dao;

import in.xebia.ems.dao.generic.GenericDAO;
import in.xebia.ems.domain.Project;

/**
 *
 * @author rocky
 */

public interface ProjectDAO extends GenericDAO<Project, Long>{
}
