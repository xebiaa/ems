package in.xebia.ems.dao.generic;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author rocky
 */
public interface GenericDAO<T, ID extends Serializable> {

	List<T> findAll();

	T findById(ID id);

	T load(ID id);

	void deleteById(ID id);

	T persist(T entity);

	T merge(T entity);

	void delete(T entity);
}
