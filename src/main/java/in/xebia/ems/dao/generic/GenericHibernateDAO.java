package in.xebia.ems.dao.generic;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

/**
 *
 * @author rocky
 * @author Mukesh Shah
 */
@Repository
public abstract class GenericHibernateDAO<T, ID extends Serializable> implements GenericDAO<T, ID> {

    private final Class<T> persistentClass;

    private String entityName;
    
    @PersistenceContext
    private EntityManager entityManager;
    
    public GenericHibernateDAO() {
        this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        this.entityName = getEntityName();
    }

	public List<T> findAll() {
        Query query = getEntityManager().createQuery("SELECT o FROM "+ getEntityName() + " o");
        return query.getResultList();
    }
	
    public List<T> findByFields(String field, Object value) {
        Query query = getEntityManager().createQuery("SELECT o FROM "+ getEntityName() + " o " + " WHERE " + field  + " = ? ");
        query.setParameter(1, value);
        return query.getResultList();
    }
    
    public final T findById(ID id){
    	return entityManager.find(persistentClass, id);
    }

    public final T load(ID id) {
        return getEntityManager().getReference(persistentClass,id);
    }


    public final void deleteById(ID id) {
        T obj = this.findById(id);
        if (obj != null) {
            entityManager.remove(obj);
        }
    }

    public T persist(T entity) {
        entityManager.persist(entity);
        return entity;
    }

    public T merge(T entity) {
        return entityManager.merge(entity);
    }
    
    public void delete(T entity) {
        entityManager.remove(entity);
    }

    protected final String getEntityName() {
        if (entityName == null) {
            Entity entityAnn = persistentClass.getAnnotation(Entity.class);

            if (entityAnn != null && !entityAnn.name().equals("")) {
                entityName = entityAnn.name();
            } else {
                entityName = persistentClass.getSimpleName();
            }
        }
        return entityName;
    }

    protected final EntityManager getEntityManager() {
        return  entityManager;
    }
}
