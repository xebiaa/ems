package in.xebia.ems.dao.impl;

import in.xebia.ems.dao.EmployeeDAO;
import in.xebia.ems.dao.generic.GenericHibernateDAO;
import in.xebia.ems.domain.Employee;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

/**
 * 
 * @author rocky
 */
@Repository("employeeDAOHibernate")
public class EmployeeDAOHibernate extends GenericHibernateDAO<Employee, Long>
		implements EmployeeDAO {

	@Override
	public List<Employee> findByUsernameAndPassword(String username, String password) {
		String queryStr = "Select o from Employee o where o.username = :username and o.password = :password";
		Query query = getEntityManager().createQuery(queryStr);
		query.setParameter("username", username);
		query.setParameter("password", password);
		List<Employee> employees = query.getResultList();
		return employees;
	}
	
	public List<Employee> listAllActiveEmployees() {
        Query query = getEntityManager().createQuery("SELECT o FROM Employee o WHERE o.active=TRUE");
        return query.getResultList();
    }

}
