package in.xebia.ems.dao.impl;

import org.springframework.stereotype.Repository;

import in.xebia.ems.dao.RoleDAO;
import in.xebia.ems.dao.generic.GenericHibernateDAO;
import in.xebia.ems.domain.Role;

@Repository
public class RoleDAOHibernate extends GenericHibernateDAO<Role, Long> implements RoleDAO{

}
