package in.xebia.ems.dao.impl;

import in.xebia.ems.dao.ProjectDAO;
import in.xebia.ems.dao.generic.GenericHibernateDAO;
import in.xebia.ems.domain.Project;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

/**
 * 
 * @author rocky
 */
@Repository("projectDAOHibernate")
public class ProjectDAOHibernate extends GenericHibernateDAO<Project, Long>
		implements ProjectDAO {

	public List<Project> findByProjectName(String projectName) {
		String queryStr = "Select o from Project o where o.projectName = :projectName";
		Query query = getEntityManager().createQuery(queryStr);
		query.setParameter("projectName", projectName);
		List<Project> projects = query.getResultList();
		return projects;
	}
}
