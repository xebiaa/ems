package in.xebia.ems.dao;

import java.util.List;

import in.xebia.ems.dao.generic.GenericDAO;
import in.xebia.ems.domain.Employee;

/**
 *
 * @author rocky
 */

public interface EmployeeDAO extends GenericDAO<Employee, Long>{
	List<Employee> findByUsernameAndPassword(String username, String password);
}
