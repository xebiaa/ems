package in.xebia.ems.dao;

import in.xebia.ems.dao.generic.GenericDAO;
import in.xebia.ems.domain.Role;

public interface RoleDAO extends GenericDAO<Role, Long> {

}
